if status is-login
    set -U fish_greeting
    set -gx EDITOR "emacsclient -a emacs"
    set -gx VISUAL $EDTIOR

    for pkg in openssl sqlite libinput libxkbcommon glib freetype
        set -gx LD_LIBRARY_PATH "$LD_LIBRARY_PATH:$(guix package -I ^$pkg\$ | cut -f4)/lib"
    end
    set -gx LD_LIBRARY_PATH "$LD_LIBRARY_PATH:/gnu/store/v268li21yk9l6vqiq2mrw6vvpx2bnm5c-gcc-11.3.0-lib/lib"

    fish_add_path $HOME/.roswell/bin
    fish_add_path $HOME/.local/bin
    fish_add_path $HOME/.pulumi/bin

    # TODO: Handle the two main cases of being docked or at a laptop.
    xmodmap $HOME/.Xmodmap
end

alias cfg="git --git-dir=$HOME/git/cvcf/dotfiles --work-tree=$HOME "
alias tf=terraform
