(in-package #:nyxt-user)

(define-configuration browser
  ((session-restore-prompt :always-ask)))

(define-configuration buffer
  ((default-modes (append '(vi-normal-mode dark-mode) %slot-default%))))

(define-configuration web-buffer
  ((default-modes (append '(blocker-mode reduce-tracking-mode force-https-mode) %slot-default%))))

(define-configuration input-buffer
  ((default-modes (cons 'vi-insert-mode %slot-default%))))

(define-configuration prompt-buffer
  ((override-map (let ((om (make-keymap "prompt-buffer-override")))
                   (define-key %slot-default%
                     "C-g" 'nothing)))))
