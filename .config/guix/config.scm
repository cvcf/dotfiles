(use-modules (gnu)
             (ice-9 optargs)
             (srfi srfi-1)
             (gnu packages shells)
             (gnu packages android)
             (gnu services sysctl)
             (guix inferior)
             (guix channels)
             (nongnu packages linux)
             (nongnu system linux-initrd))

(use-service-modules admin
                     cups
                     desktop
                     docker
                     networking
                     nix
                     ssh
                     virtualization
                     vpn
                     xorg)

(define cc/nonguix-signing-key "(public-key
                                 (ecc
                                  (curve Ed25519)
                                  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))")

(operating-system
 (locale "en_US.utf8")
 (timezone "America/Chicago")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "sagan")

 (kernel
  (let ((inferior (inferior-for-channels
                   (list (channel
                          (name 'nonguix)
                          (url "https://gitlab.com/nonguix/nonguix")
                          (commit "9d58bb6e3e93a444d35bf62d4ca98a126ae4f295"))
                         (channel
                          (name 'guix)
                          (url "https://git.savannah.gnu.org/git/guix.git")
                          (commit "0584f5b48987c058e2dd694c2bc886b7aa40e15d"))))))
    (first (lookup-inferior-packages inferior "linux" "6.2.14"))))
 (initrd microcode-initrd)
 (initrd-modules (cons* "br_netfilter" (delete "framebuffer_coreboot" %base-initrd-modules)))
 (firmware (cons* iwlwifi-firmware %base-firmware))

 ;; User accounts
 (users (cons* (user-account
                (name "cvc")
                (comment "Cameron Chaparro")
                (group "users")
                (home-directory "/home/cvc")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video" "lp" "docker" "kvm" "adbusers" "dialout" "bluetooth"))
                (shell (file-append fish "/bin/fish")))
               %base-user-accounts))

 ;; Globally-installed packages (packages
 (packages (append
            (map specification->package
                 '(;; Emacs Desktop
                   "emacs-next"
                   "emacs-desktop-environment"

                   ;; StumpWM
                   "stumpwm-with-slynk"
                   "emacs-stumpwm-mode"
                   "sbcl-stumpwm-winner-mode"
                   "sbcl-stumpwm-swm-gaps"
                   "sbcl-stumpwm-screenshot"
                   "sbcl-stumpwm-pass"
                   "sbcl-stumpwm-kbd-layouts"
                   "sbcl-stumpwm-globalwindows"
                   "sbcl-stumpwm-wifi"
                   "sbcl-stumpwm-ttf-fonts"
                   "sbcl-stumpwm-stumptray"
                   "sbcl-stumpwm-net"
                   "sbcl-stumpwm-mem"
                   "sbcl-stumpwm-cpu"
                   "stumpish"

                   ;; CA Certs
                   "nss-certs"

                   ;; General
                   "fish"
                   "zsh"
                   "ncurses"))
            %base-packages))

 ;; System services
 (services
  (append (list
           (service block-facebook-hosts-service-type)

           ;; Bluetooth
           ;; For whatever reason it's auto-enabling it...
           (service bluetooth-service-type
                    (bluetooth-configuration
                     (auto-enable? #t)))

           ;; Cups
           (service cups-service-type
                    (cups-configuration
                     (web-interface? #t)))

           ;; Docker
           (service docker-service-type)

           ;; OpenSSH
           (service openssh-service-type
                    (openssh-configuration
                     (x11-forwarding? #t)))

           ;; VPN
           (service wireguard-service-type
                    (wireguard-configuration
                     (addresses '("10.5.5.2/32"))
                     (dns '("1.1.1.1" "1.0.0.1"))
                     (peers
                      (list
                       (wireguard-peer
                        (name "vpn.chaparro.io")
                        (endpoint "vpn.chaparro.io:51820")
                        (public-key "Edjsd71bhP4r7ZrndTQXcDpkktT8n+/e5jSqpLpZdx8=")
                        (allowed-ips '("0.0.0.0/0")))))
                     (post-up '("iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE"))
                     (post-down '("iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE"))))


           ;; Unattended upgrades
           (service unattended-upgrade-service-type
                    (unattended-upgrade-configuration
                     (services-to-restart
                      '(mcron ntpd ssh-daemon vpn-client))))

           ;; udev rules
           (udev-rules-service 'android android-udev-rules
                               #:groups '("adbusers"))

           ;; Xorg
           (set-xorg-configuration
            (xorg-configuration
             (keyboard-layout keyboard-layout))))

          (modify-services %desktop-services
                           ;; Disable IPv6
                           (sysctl-service-type config =>
                                                (sysctl-configuration
                                                 (settings (append '(("net.ipv6.conf.all.disable_ipv6" . "1")
                                                                     ("net.ipv6.conf.default.disable_ipv6" . "1"))
                                                                   %default-sysctl-settings))))
                           ;; Configure the nonguix substitute
                           (guix-service-type config =>
                                              (guix-configuration
                                               (inherit config)
                                               (substitute-urls
                                                (append (list "https://substitutes.nonguix.org")
                                                        %default-substitute-urls))
                                               (authorized-keys
                                                (append (list (plain-file "nonguix.pub"
                                                                          cc/nonguix-signing-key))
                                                        %default-authorized-guix-keys)))))))

 ;; Bootloader config
 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets '("/boot/efi"))
   (keyboard-layout keyboard-layout)
   (timeout 2)))

 ;; Encrypted devices
 (mapped-devices
  (list (mapped-device
         (source (uuid "1719bb5c-7258-4eb9-828d-5f4542fba9f7"))
         (target "crypthome")
         (type luks-device-mapping))
        (mapped-device
         (source (uuid "c798d089-892b-4d42-b0b1-e665300b556a"))
         (target "cryptroot")
         (type luks-device-mapping))))

 ;; File systems
 (file-systems
  ;; TODO: LUKS/LVM don't work together, yet
  (cons* (file-system
          (mount-point "/home")
          (device "/dev/mapper/crypthome")
          (type "ext4")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/")
          (device "/dev/mapper/cryptroot")
          (type "ext4")
          (dependencies mapped-devices))
         (file-system
          (mount-point "/boot/efi")
          (device (uuid "2254-8A76" 'fat32))
          (type "vfat"))
         %base-file-systems)))
