# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source /etc/bashrc

alias ls='ls --color=auto'
alias diff='diff --color=auto'
alias grep='egrep --color=auto'
alias dmesg='dmesg --color=always'
alias k='kubectl'
alias tf='terraform'

PS1='[\u@\h \W]\$ '

export GPG_TTY=$(tty)

export home=$HOME
export backup_dir=/run/mount/backup/sagan/cvc

alias bu="backup.sh -b $backup_dir/ -e $home/excludes -f $home/includes -i $home/includes $home/"

alias cfg="$(which git) --git-dir=$HOME/git/cvcf/dotfiles --work-tree=$HOME"
