(defun cvcf/imap-config (name)
  `(nnimap ,name
           (nnimap-address "imap.gmail.com")
           (nnimap-server-port "imaps")
           (nnimap-stream ssl)
           (nnimap-expunge on-exit)
           (nnmail-expiry-target ,(concat "nnimap+" name ":[Gmail]/Trash"))
           (nnmail-expiry-wait 90)))

(defun cvcf/posting-style (name email &optional organization)
  `(,name
    (header "to" ,email)
    (address ,(concat user-full-name " <" email ">"))
    ,@(when organization
        `((organization ,organization)))
    ("X-Message-SMTP-Method" ,(concat "smtp smtp.gmail.com 587 " email))))

(defun cvcf/gnus-group-list-subscribed-groups ()
  "List all subscribed groups regardless of whether there are un-read messages."
  (interactive)
  (gnus-group-list-all-groups))

(define-key gnus-group-mode-map
  (kbd "o") 'cvcf/gnus-group-list-subscribed-groups)

(require 'nnir)

(add-to-list 'nnir-imap-search-arguments '("gmail" . "X-GM-RAW"))

(require 'bbdb)

(bbdb-initialize 'message 'gnus 'sendmail)
(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
(setq bbdb/mail-auto-create-p t
      bbdb/news-auto-create-p t)

(add-hook 'message-mode-hook
          (lambda ()
            (flyspell-mode t)
            (local-set-key (kbd "TAB") 'bbdb-complete-name)))

(setq gnus-select-method '(nnnil nil)
      gnus-secondary-select-methods (mapcar 'cvcf/imap-config '("per" "ccc" "ccg"))

      gnus-summary-next-group-on-exit nil

      ;; Don't save a .newsrc file (since I don't even have one)
      gnus-save-newsrc-file nil

      gnus-message-archive-group '(("cvchaparro@gmail.com"
                                    "nnimap+per:[Gmail]/Sent Mail")
                                   ("cameron@cameronchaparro.com"
                                    "nnimap+ccc:[Gmail]/Sent Mail")
                                   ("cameron@chaparroconsulting.group"
                                    "nnimap+ccg:[Gmail]/Sent Mail"))

      gnus-posting-styles (mapcar* 'cvcf/posting-style
                                   '("per" "ccc" "ccg")
                                   '("cvchaparro@gmail.com"
                                     "cameron@cameronchaparro.com"
                                     "cameron@chaparroconsulting.group")
                                   '(nil nil "Chaparro Consulting Group, LLC"))

      gnus-extra-headers '(To Newsgroups X-GM-LABELS)

      gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-date
                                   (not gnus-thread-sort-by-number))

      gnus-use-cache t)

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587)

(setq nntp-authinfo-file "~/.authinfo.gpg")

(setq nnml-directory "~/docs/mail"
      nnml-active-file "~/docs/mail/active"
      nnml-newsgroups-file "~/docs/mail/newsgroups")

(setq message-directory "~/docs/mail")

(setq nnimap-record-commands t)

(eval-after-load 'mailcap
  (mailcap-parse-mailcaps))

(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

(setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject
      gnus-thread-hide-subtree t
      gnus-thread-ignore-subject t)
