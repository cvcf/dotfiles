[[ -f ~/.bashrc ]] && . ~/.bashrc

export LESS=-R
export LESS_TERMCAP_mb=$'\e[1;31m'
export LESS_TERMCAP_md=$'\e[1;36m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;44;33m'
export LESS_TERMCAP_us=$'\e[1;32m'
export LESS_TERMCAP_ue=$'\e[0m'
export EDITOR=emacsclient
export VISUAL=emacsclient
export GIT_EDITOR=$VISUAL
export PYTHONDONTWRITEBYTECODE='.'

export AWS_ACCESS_KEY_ID=$(tail -n1 $HOME/.aws/auth | cut -d, -f3)
export AWS_SECRET_ACCESS_KEY=$(tail -n1 $HOME/.aws/auth | cut -d, -f4)
export AWS_REGION="us-east-2"

export PEM_FILE_LOCATION=$HOME/.ssh/aws.pem

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:-.}:$(guix package -I openssl | cut -f4)/lib

export PATH=$PATH:$HOME/.roswell/bin

xmodmap $HOME/.Xmodmap
